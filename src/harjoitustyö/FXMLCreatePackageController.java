/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harjoitustyö;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Omistaja
 */
public class FXMLCreatePackageController implements Initializable {

    @FXML
    private TextField inputNameField;
    @FXML
    private ComboBox<String> departureCityCombo;
    @FXML
    private ComboBox<SmartPost> departureLocationCombo;
    @FXML
    private ComboBox<String> arriveCityCombo;
    @FXML
    private ComboBox<SmartPost> arriveLocationCombo;
    @FXML
    private Button createButton;
    @FXML
    private CheckBox fragileChooseBox;
    @FXML
    private RadioButton firstClass;
    @FXML
    private ToggleGroup classGroup;
    @FXML
    private RadioButton secondClass;
    @FXML
    private RadioButton thirdClass;
    @FXML
    private Button infoButton;
    @FXML
    private Button cancelButton;
    private ArrayList<SmartPost> availableLocations;
    @FXML
    private ComboBox<String> sizeCombo;
    @FXML
    private TextArea outputTextArea;
    @FXML
    private ComboBox<String> weightCombo;
    @FXML
    private ComboBox<Item> itemsCombo;
    @FXML
    private Button createNewItemButton;
    @FXML
    private WebView web2;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        /*
        Initializes 4 different kind of items. Initializes also the values for the
        RadioButtons, weightclass and sizeclass selections.
        */
        sizeCombo.getItems().addAll("Pieni", "Keskisuuri", "Suuri", "Erittain suuri");
        weightCombo.getItems().addAll("A (0 - 0.5 kg)", "B (0.5 - 2 kg)", "C (2 - 10kg)", "D (10 - 50 kg)");
        firstClass.setUserData("1");
        secondClass.setUserData("2");
        thirdClass.setUserData("3");
        /*
        Fires up the "index.html" into Webview which is hudded. The only purpose is to 
        later exetecute script to check the distance between two SmartPosts.
        */
        web2.getEngine().load(getClass().getResource("index.html").toExternalForm());
        
        itemsCombo.getItems().add(new Item("Klemmareita", "Pieni","A (0 - 0.5kg)", false));
        itemsCombo.getItems().add(new Item("24 BlueRay BoxSet", "Pieni","A (0 - 0.5kg)",  true));
        itemsCombo.getItems().add(new Item("Keilapallo", "Keskisuuri" ,"C (2 - 10kg)", false));
        itemsCombo.getItems().add(new Item("20kg Biolan mustaa multaa", "Erittäin suuri" , "D (10 - 50kg)", false));
        
    }
    
    public void updateLocations(ArrayList<SmartPost> locations) {
        /*
        With this method added SmartPost locations can be updated every time the
        the window is opened.
        */
        ArrayList<String> tempList = new ArrayList<>();
        tempList.add("");
        for(int i = 0; i < locations.size(); i++) {
            
            boolean a = tempList.contains(locations.get(i).getCity());
            if(a == false) {
            departureCityCombo.getItems().add(locations.get(i).getCity());
            arriveCityCombo.getItems().add(locations.get(i).getCity());
            tempList.add(locations.get(i).getCity());
            }
        }
        availableLocations = locations;
    }

    @FXML
    private void createAction(ActionEvent event) {
        /*
        This method creates the new package and stores it to Storage class.
        */
        boolean a = itemsCombo.valueProperty().getValue() == null;
        boolean b = classGroup.getSelectedToggle() == null;
        boolean c = departureLocationCombo.valueProperty().getValue() == null;
        boolean d = arriveLocationCombo.valueProperty().getValue() == null;
        boolean e = departureLocationCombo.valueProperty().getValue() ==
                arriveLocationCombo.valueProperty().getValue();
        /*
        Checks that all needed inputs are given.
        */
        if(e == true) {
            outputTextArea.setText("Lähetysautomaatin täytyy olla eri kuin saapumisautomaatti!");
        }
        if(a == true && b == true) {
            outputTextArea.setText("Valitse esine ja pakettiluokka!");
        }
        if(a == true && b == false) {
            outputTextArea.setText("Valitse esine!");
        }
        if(c == true || d == true) {
            outputTextArea.setText("Lähetystiedot ovat puutteelliset!");
        }
        if(a == false && b == true) {
            outputTextArea.setText("Valitse pakettiluokka!");
        }
        
        if(a == false && b == false && c == false && d == false && e == false) {
            /*
            Checks if the package fullfills all given conditions for different packageclasses.
            Gives information to user about the packages created or displays error message if
            conditions aren't met.
            */
            Item selectedItem = itemsCombo.valueProperty().getValue();
            String selectedPackageClass = classGroup.getSelectedToggle().getUserData().toString();
            String name = selectedItem.getName();
            String size = selectedItem.getSize();
            String weight = selectedItem.getWeight();
            boolean fragile = selectedItem.getFragile();
            SmartPost dLocation = departureLocationCombo.valueProperty().getValue();
            SmartPost aLocation = arriveLocationCombo.valueProperty().getValue();
            float dLat = dLocation.getLatitude();
            float dLng = dLocation.getLongitude();
            float aLat = aLocation.getLatitude();
            float aLng = aLocation.getLongitude();
            Storage storage = null;
            storage = Storage.getInstance();
            
            ArrayList<Float> coordinates = new ArrayList<>();
            coordinates.add(dLat);
            coordinates.add(dLng);
            coordinates.add(aLat);
            coordinates.add(aLng);                
            String command = "document.createPath(" + coordinates + ",'red',1)";
           float distance = Float.parseFloat(web2.getEngine().executeScript(command).toString());
            
            if(selectedPackageClass.equals("1")) {
                // First class checks
                if(fragile == true) {
                    outputTextArea.setText("1. Luokan pakettien sisältö ei saa olla särkyvää");
                }
                else{
                    /*
                    First class packages cannot be send further tha 150 kilometres
                    This need to be checked by executing script "document.createPath()".
                    */
                    if(distance < 150 ) {
                    FirstClass newPackage = new FirstClass(selectedItem, dLocation, aLocation, distance);
                    storage.addPackageToStorage(newPackage);
                    outputTextArea.setText("Luotiin uusi paketti:\n" + 
                    "Pakettiluokka: 1. Luokka\n" + "Sisältö: " + name + "\n" + 
                    "Koko: " + size + "\nPainoluokka: " + weight);
                    }
                    else{
                        outputTextArea.setText("1. Luokan pakettia ei voi lähettää yli " + 
                                "150 kilometrin päähän!");
                    }
                }
            }
            if(selectedPackageClass.equals("2")) {
                // Second class case
                boolean aa = (size.endsWith("Pieni") || size.equals("Keskisuuri"));
                boolean bb = (weight.contains("A") || weight.contains("B"));
                
                if(fragile == true) {
                    /*
                    Fragile items have to met certains size and weight requirements.
                    */
                    if(aa == true && bb == true) {
                        SecondClass newPackage = new SecondClass(selectedItem, dLocation, aLocation, distance);
                        storage.addPackageToStorage(newPackage);
                        outputTextArea.setText("Luotiin uusi paketti:\n" + 
                        "Pakettiluokkka: 2. Luokka\n" + "Sisältö: " + name + "\n" +
                        "Koko: " + size + "\nPainoluokka: " + weight + "\n" + 
                        selectedItem.getToughness());
                    }
                    else{
                        outputTextArea.setText("Valitsemaasi sätrkyvää esinettä ei voi" +
                        "lähettää 2. pakettiluokassa.\nSaat lisätietoa painamalla" + 
                        " \"Infoa luokista\"-painiketta." );
                    }
                }
                else{
                    SecondClass newPackage = new SecondClass(selectedItem, dLocation, aLocation, distance);
                    storage.addPackageToStorage(newPackage);
                    outputTextArea.setText("Luotiin uusi paketti:\n" + 
                    "Pakettiluokkka: 2. Luokka\n" + "Sisältö: " + name + "\n" +
                    "Koko: " + size + "\nPainoluokka: " + weight + "\n" + 
                    selectedItem.getToughness());
                }
            }
            if(selectedPackageClass.equals("3")) {
                // Third class case
                if(fragile == true) {
                    boolean cc = (size.equals("Suuri") || size.equals("Erittäin suuri"));
                    boolean dd = weight.contains("D");
                    
                    if(cc == true && dd == true) {
                        /*
                        Fragile items also have to meet certain size and weight requirements.
                        */
                        ThirdClass newPackage = new ThirdClass(selectedItem, dLocation, aLocation, distance);
                        storage.addPackageToStorage(newPackage);
                        outputTextArea.setText("Luotiin uusi paketti:\n" + 
                    "Pakettiluokkka: 3. Luokka\n" + "Sisältö: " + name + "\n" +
                    "Koko: " + size + "\nPainoluokka: " + weight + "\n" + 
                    selectedItem.getToughness());
                    }
                    else{
                         outputTextArea.setText("Valitsemaasi sätrkyvää esinettä ei voi" +
                        "lähettää 2. pakettiluokassa.\nSaat lisätietoa painamalla" + 
                        " \"Infoa luokista\"-painiketta." );
                    }
                }
                else{
                    ThirdClass newPackage = new ThirdClass(selectedItem, dLocation, aLocation, distance);
                    storage.addPackageToStorage(newPackage);
                    outputTextArea.setText("Luotiin uusi paketti:\n" + 
                    "Pakettiluokkka: 3. Luokka\n" + "Sisältö: " + name + "\n" +
                    "Koko: " + size + "\nPainoluokka: " + weight + "\n" + 
                    selectedItem.getToughness());
                }
            }
            
            
        }
        
    }

    @FXML
    private void cancelAction(ActionEvent event) {
        // Closes the package creation window
        Stage stage = (Stage) cancelButton.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void infoAction(ActionEvent event) {
        // Opens up window which displays ingormation to user about different package classes.
        Stage stage = new Stage();
        try {
            Parent page = FXMLLoader.load(getClass().getResource("FXMLInfoWindow.fxml"));
            Scene Scene = new Scene(page);
            stage.setScene(Scene);
            stage.show();
            
        } catch (IOException ex) {
            Logger.getLogger(FXMLCreatePackageController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void listAvailableDepartureLocations(ActionEvent event) {
        /*
        Lists the available SmartPosts in the celected city.
        */
        departureLocationCombo.getItems().clear();
        String chosenDepartureCity = departureCityCombo.valueProperty().getValue();
        for(int i = 0; i < availableLocations.size(); i++) {
            if(availableLocations.get(i).getCity().equals(chosenDepartureCity)) {
                departureLocationCombo.getItems().add(availableLocations.get(i));
            }
        }
    }

    @FXML
    private void listAvailableArriveLocations(ActionEvent event) {
        /*
        Lists all available SmartPosts in the selected city.
        */
        arriveLocationCombo.getItems().clear();
        String chosenArrivalCity = arriveCityCombo.valueProperty().getValue();
        for(int i = 0; i < availableLocations.size(); i++) {
            if(availableLocations.get(i).getCity().equals(chosenArrivalCity)) {
                arriveLocationCombo.getItems().add(availableLocations.get(i));
            }
        }
    }

    @FXML
    private void createNewItemAction(ActionEvent event) {
        /*
        Enables user to create owns items.
        */
        boolean a = inputNameField.getText().isEmpty();
        boolean b = sizeCombo.valueProperty().getValue() == null;
        boolean c = weightCombo.valueProperty().getValue() == null;
        boolean isFragile = fragileChooseBox.selectedProperty().get();
        /*
        Displays error message or creates new item depending if the user has
        given all needed inputs.
        */
        if(a == true || b == true || c == true) {
            outputTextArea.setText("Anna kaikki uuden esineen tiedot!");
        }
        else{
            String name = inputNameField.getText();
            String size = sizeCombo.valueProperty().getValue();
            String weight = weightCombo.valueProperty().getValue();
            Item newItem = new Item(name, size, weight, isFragile);
            itemsCombo.getItems().add(newItem);
            
            outputTextArea.setText("Uusi esine luotu!\n" + "Nimi: " + name + "\n" +
            "Koko: " + size + "\nPainoluokka; " + weight + "\n" + newItem.getToughness());
        }
    }

    
    
}
