/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harjoitustyö;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Omistaja
 */
public class FXMLStatisticsController implements Initializable {

    @FXML
    private TextArea historyTextArea;
    @FXML
    private TextArea storageTextArea;
    @FXML
    private Button closeButton;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        /*
        Displays to user the amount og packages currently in storage and the
        amount of packages already been sent during the run of the program so far.
        Also displays information about the packages.
        */
     Storage storage = null;
     storage = Storage.getInstance();
     ArrayList<Package> history = storage.getPackageHistory();
     ArrayList<Package> inStorage = storage.getPackageList();
     
     historyTextArea.setText("Tähän mennessä lähetetty yhteensä " + history.size() + 
             " pakettia.\nLähetetyt paketit ja niiden tiedot:\n");
     
     for(int i = 0; i < history.size(); i++) {
         Package pkg = history.get(i);
         float distance = pkg.getDistance();
         SmartPost d = pkg.getDepartureLocation();
         SmartPost a = pkg.getArrivalLocation();
         String dCity = d.getCity();
         String aCity = a.getCity();
         String dAddress = d.getAddress();
         String aAddress = a.getAddress();
         Item item = pkg.getItem();
         String name = item.getName();
         String size = item.getSize();
         String weight = item.getWeight();
         String toughness = item.getToughness();
         
         historyTextArea.setText(historyTextArea.getText() + "Mistä; " + 
                 dCity + " " + dAddress + "\nMinne: " + aCity + " " + aAddress +
                 "\nEtäisyys: " + distance + "km\nKoko: " + size + "\nPainoluokka: " + 
                 weight + "\nSisältö: " + name + " (" + toughness + ")" +
                 "\n_________________________________________\n");
         
     }
     
     storageTextArea.setText("Varastossa on tällä hetkellä " + inStorage.size() +
             " pakettia.\n");
     
     for(int j = 0; j < inStorage.size(); j++) {
         Package pkg = inStorage.get(j);
         float distance = pkg.getDistance();
         SmartPost d = pkg.getDepartureLocation();
         SmartPost a = pkg.getArrivalLocation();
         String dCity = d.getCity();
         String aCity = a.getCity();
         String dAddress = d.getAddress();
         String aAddress = a.getAddress();
         Item item = pkg.getItem();
         String name = item.getName();
         String size = item.getSize();
         String weight = item.getWeight();
         String toughness = item.getToughness();
         
         storageTextArea.setText(storageTextArea.getText() + "Mistä; " + 
                 dCity + " " + dAddress + "\nMinne: " + aCity + " " + aAddress +
                 "\nEtäisyys: " + distance + "km\nKoko: " + size + "\nPainoluokka: " + 
                 weight + "\nSisältö: " + name + " (" + toughness + ")" +
                 "\n_________________________________________\n");
         
     }
     
    } 

    @FXML
    private void closeWindowAction(ActionEvent event) {
        // Closes the window
    Stage stage = (Stage) closeButton.getScene().getWindow();
        stage.close();
    }
    
    
}
