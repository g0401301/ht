/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harjoitustyö;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.ComboBox;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author Topi
 */
public class DataBuilder {
    
    private Document doc;
    private ArrayList<SmartPost> boxes = new ArrayList<>();
    
    public DataBuilder(ComboBox combo) {
        /*
        ComboBox is given as parametre in order to list all the SmartPosts in the
        main window.
        */
        
        try {
            /*
            DataBuilders function is read information about all the SmartPosts in Finland
            in xml form and parse thay information. This class also creates all the SmartPosts
            based on the information it reads.
            */
            URL url = new URL("http://smartpost.ee/fi_apt.xml");
            BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
            String content = "";
            String line;
            
            while((line = br.readLine()) != null) {
            content += line + "\n";
            }
            
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            
            doc = dBuilder.parse(new InputSource(new StringReader(content)));
            doc.getDocumentElement().normalize();
            
            parseCurrentData(combo);
            
        } catch (MalformedURLException ex) {
            Logger.getLogger(DataBuilder.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DataBuilder.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException | SAXException ex) {
            Logger.getLogger(DataBuilder.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
        
        
    
     private void parseCurrentData(ComboBox cb) {
         /*
         This function parses the data and based on that creates the SmartPosts.
         Also the SmartPosts are added to ComboBox in the main window.
         */
     NodeList nodes = doc.getElementsByTagName("place");
     for(int i = 0; i < nodes.getLength(); i++) {
     Node node = nodes.item(i);
     Element e = (Element) node;
     
     String code = getValue("code", e);
     String city = getValue("city", e);
     String address = getValue("address", e);
     String availability = getValue("availability", e);
     String postOffice = getValue("postoffice", e);
     float lat = Float.parseFloat(getValue("lat", e));
     float lng = Float.parseFloat(getValue("lng", e));
     
     SmartPost temp = new SmartPost(code, city, address, availability, postOffice, lat, lng);
     boxes.add(temp);
     cb.getItems().add(temp);
     
     
     }
     }   
    
private String getValue(String tag, Element e) {
return ((Element)e.getElementsByTagName(tag).item(0)).getTextContent();
}
     
}