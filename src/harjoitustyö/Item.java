/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harjoitustyö;

/**
 *
 * @author Omistaja
 */
public class Item {
    private String name;
    private String size;
    private String weight;
    private boolean fragile;
    private String toughness;
    
    public Item(String a, String b, String c, boolean d) {
        name = a;
        size = b;
        weight = c;
        fragile = d;
        if(d == false) {
            toughness = "Ei särkyvä";
        }
        if(d == true) {
            toughness = "Särkyvä";
        }
    }
    public String toString() {
        /*
        Parses info about the item to wanted form to be show in ComboBox containing
        Items.
        */
        String info = name + " " + size+ " " + weight + " " + toughness;
        return info;
    }
    /*
    Below there are the basic getters for the Item class.
    */
    public String getName() {
        return name;
    }
    public String getSize() {
        return size;
    }
    public String getWeight() {
        return weight;
    }
    public boolean getFragile() {
        if(fragile == true) {
        return true;
        }
        else{
        return false;
        }
    }
        public String getToughness() {
            return toughness;
        }
    }

