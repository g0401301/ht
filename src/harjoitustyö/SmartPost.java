/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harjoitustyö;

/**
 *
 * @author Topi
 */
public class SmartPost {
    private String code;
    private String city;
    private String address;
    private String availability;
    private String postOffice;
    private float lat;
    private float lng;
    
    public SmartPost(String a, String b, String c, String d, String e, float f, float g) {
    code = a;
    city = b;
    address = c;
    availability = d;
    postOffice = e;
    lat = f;
    lng = g;
    }
    /*
    Below are the basic getters for SmartPost class.
    */
    public String getPostalCode() {
        return code;
    }
    
    public String getCity() {
        return city;
    }
    
    public String getAddress() {
        return address;
    }
    
    public String getAvailability() {
        return availability;
    }
    
    public String getPostOffice() {
        return postOffice;
    }
    
    public float getLatitude() {
        return lat;
    }
    
    public float getLongitude() {
        return lng;
    }

public String toString() {
    /*
    Parses the info about SmartPost to be shown in ComboBox.
    */
    String info = city + " " + address;
return info;
}

}
