/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harjoitustyö;

import java.util.ArrayList;

/**
 *
 * @author Omistaja
 */
public class Storage {
    private ArrayList<Package> packages;
    private ArrayList<Package> packageHistory;
    static private Storage storage = null;
    
    private Storage() {
        packages = new ArrayList<>();
        packageHistory = new ArrayList<>();
    }
    /*
    Storage is a Singleton case. This because it contains the list of the created packages
    in order to avoid any problems.
    */
    static public Storage getInstance() {
        if(storage == null) {
            storage = new Storage();
        }
        return storage;
    }
    
    public void addPackageToStorage(Package p) {
        packages.add(p);
    }
    public void removePackageFromStorage(Package p) {
        /*
        This method removes wanted package from the storage and adds it to
        list which contains information about all of the packages that have been sent.
        */
        packageHistory.add(p);
        for(int i = 0; i < packages.size(); i++) {
            if(packages.get(i).equals(p)) {
                packages.remove(i);
            }
        }
    }
    public ArrayList getPackageHistory() {
        // Returns the list containing already sent packages
        return packageHistory;
    }
    public ArrayList getPackageList() {
        // Returns the list containing packages currently in storage.
        return packages;
    }
}
