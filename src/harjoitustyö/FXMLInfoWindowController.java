/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harjoitustyö;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Omistaja
 */
public class FXMLInfoWindowController implements Initializable {

    @FXML
    private Button closeButton;
    @FXML
    private TextArea infoTextArea;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        /*
        Displays information about the limitations of the different package classes 
        to user in a TextArea.
        */
        infoTextArea.setText("1. Luokka: ´\n" + "1. Luokka on kaikista nopein, mutta"
                + " valitettavasti 1. luokan paketin voi lähettää vain 150 kilonetrin"
                + " päähän. Ensimmäisessä luokassa on mahdollista lähettää käiken kokoisia ja"
                + " painoisia paketteja, mutta be eivät saa sisältää mitään särkyvää.\n"
                + "2. Luokka: \n" + "2. luokan paketteja voi lähettää koko Suomen alueelle. Toisen "
                + "luokan paketeissa voi lähettää särtkyvää tavaraa kunhan paketin koko on joko pieni"
                + " tai keskisuuri. Painoluokka saa olla A tai B (0 - 2kg).\n"
                + "3. Luokka: \n" + "Kolmannen luokan paketit ovat kaikista hitaimpia. Lisäksi"
                + " vain suuret ja erittäin suuret paketit, jotka kuuluvat painoluokkaan D (10 - 50kg) "
                + "voivat sisältää särkyvää tavaraa.");
    }    

    @FXML
    private void closeAction(ActionEvent event) {
        Stage stage = (Stage) closeButton.getScene().getWindow();
        stage.close();
    }
    
}
