/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harjoitustyö;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;

/**
 * FXML Controller class
 *
 * @author Omistaja
 */
public class FXMLReceiptController implements Initializable {

    @FXML
    private TextArea historyTextArea;
    @FXML
    private TextArea storageTextArea;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        /*
        Displays all packeges left in storage and all the packages which were send 
        during the run of the program. All the information this window displays are also 
        daved into "receipt.txt" file.
        */
        Storage storage = null;
     storage = Storage.getInstance();
     ArrayList<Package> history = storage.getPackageHistory();
     ArrayList<Package> inStorage = storage.getPackageList();
     
     historyTextArea.setText("Paketteja lähetettiin yhteensä " + history.size() + 
             " kappaletta.\n");
     
     for(int i = 0; i < history.size(); i++) {
         Package pkg = history.get(i);
         float distance = pkg.getDistance();
         SmartPost d = pkg.getDepartureLocation();
         SmartPost a = pkg.getArrivalLocation();
         String dCity = d.getCity();
         String aCity = a.getCity();
         String dAddress = d.getAddress();
         String aAddress = a.getAddress();
         Item item = pkg.getItem();
         String name = item.getName();
         String size = item.getSize();
         String weight = item.getWeight();
         String toughness = item.getToughness();
         
         historyTextArea.setText(historyTextArea.getText() + "Mistä; " + 
                 dCity + " " + dAddress + "\nMinne: " + aCity + " " + aAddress +
                 "\nEtäisyys: " + distance + "km\nKoko: " + size + "\nPainoluokka: " + 
                 weight + "\nSisältö: " + name + " (" + toughness + ")" +
                 "\n_________________________________________\n");
         
     }
     
     storageTextArea.setText("Varastoon jäi " + inStorage.size() +
             " pakettia.\n");
     
     for(int j = 0; j < inStorage.size(); j++) {
         Package pkg = inStorage.get(j);
         float distance = pkg.getDistance();
         SmartPost d = pkg.getDepartureLocation();
         SmartPost a = pkg.getArrivalLocation();
         String dCity = d.getCity();
         String aCity = a.getCity();
         String dAddress = d.getAddress();
         String aAddress = a.getAddress();
         Item item = pkg.getItem();
         String name = item.getName();
         String size = item.getSize();
         String weight = item.getWeight();
         String toughness = item.getToughness();
         
         storageTextArea.setText(storageTextArea.getText() + "Mistä; " + 
                 dCity + " " + dAddress + "\nMinne: " + aCity + " " + aAddress +
                 "\nEtäisyys: " + distance + "km\nKoko: " + size + "\nPainoluokka: " + 
                 weight + "\nSisältö: " + name + " (" + toughness + ")" +
                 "\n_________________________________________\n");
         
     }
     
    
        
    }    
    
}
