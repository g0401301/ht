/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harjoitustyö;

/**
 *
 * @author Omistaja
 */
public class Package {
    protected int postClass;
    protected Item item;
    protected SmartPost departure;
    protected SmartPost arrival;
    protected float distance;
    
    public Package(int a, Item i, SmartPost b, SmartPost c, float d) {
        postClass = a;
        item = i;
        departure = b;
        arrival = c;
        distance = d;
    }
    /*
    Below are basic getters. Needed to get hands on the packages info.
    */
    public SmartPost getDepartureLocation() {
        return departure;
    }
    public SmartPost getArrivalLocation() {
        return arrival;
    }
    public Item getItem() {
        return item;
    }
    public int getPostClass() {
        return postClass;
    }
    public float getDistance() {
        return distance;
    }
    public String toString() {
        /*
        Parses the information to wanted form. This is shown in the ComboBox 
        dropdown menu which contains Packages.
        */
        int temp1 = this.getPostClass();
        String temp2 = this.getItem().getName();
        String temp3 = this.getDepartureLocation().getCity();
        String temp4 = this.getDepartureLocation().getAddress();
        String temp5 = this.getArrivalLocation().getCity();
        String temp6 = this.getArrivalLocation().getAddress();
        String info = "Pakettiluokka: " + temp1 + " Sisältö: " + temp2 + " Mistä:" +
                temp3 + " " + temp4 + " Minne: " + temp5 + " " + temp6;
        return info;
    }
    }
    
    class FirstClass extends Package {
        
        public FirstClass(Item i, SmartPost a, SmartPost b, float c) {
            super(1, i, a, b,c);
        }
        
    }
class SecondClass extends Package {
    public SecondClass(Item i, SmartPost a, SmartPost b, float c) {
        super(2, i, a, b, c);
    }
}
class ThirdClass extends Package {
    public ThirdClass(Item i, SmartPost a, SmartPost b, float c) {
        super(3, i, a, b, c);
    }
}