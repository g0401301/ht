// Olio-ohjelmointi CT60A2410 Harjoitustyö
// Tekijät: Topi Jussinniemi  0401301 ja Tuomas Turunen 0402957
package harjoitustyö;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 *
 * @author Topi
 */
public class FXMLDocumentController implements Initializable {
    
    private Label label;
    @FXML
    private WebView web;
    @FXML
    private ComboBox<SmartPost> combo;
    @FXML
    private Button addToMapButton;
    @FXML
    private Button createPackageButton;
    @FXML
    private Button clearMapButton;
    @FXML
    private Button sendPackageButton;
    private ArrayList<SmartPost> addedLocations;
    @FXML
    private ComboBox<Package> packagesCombo;
    @FXML
    private Button updatePackagesButton;
    @FXML
    private Button statisticsButton;
    @FXML
    private Button closeAndPrintReceiptButton;
    @FXML
    private ComboBox<String> colorCombo;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // In initialization index.html is loaded to WebView component and DataBuilder is created.
        // Also initializes the available colors to colorCombo ComboBox.
        
        web.getEngine().load(getClass().getResource("index.html").toExternalForm());
        DataBuilder db = new DataBuilder(combo);
        addedLocations = new ArrayList<>();
        colorCombo.getItems().add("Sininen");
        colorCombo.getItems().add("Punainen");
        colorCombo.getItems().add("Vihreä");
        colorCombo.getItems().add("Musta");
        
        // Clears the old receipt from textfile 
        FileWriter fw;
        try {
            fw = new FileWriter("receipt.txt");
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter pw = new PrintWriter(bw);
            pw.println("KUITTI!:");
            pw.close();
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }    

    @FXML
    private void addToMapAction(ActionEvent event) {
        SmartPost selectedSmartPost;
        selectedSmartPost = combo.valueProperty().getValue();
        /*
        Adds the SmartPost which user selects from a ComboBox to a list.
        Two cases: the list is empty or already contains SmartPosts
        */
        if(addedLocations.isEmpty()){
            String postalCode = selectedSmartPost.getPostalCode();
            String address = selectedSmartPost.getAddress();
            String city = selectedSmartPost.getCity();
            String availability = selectedSmartPost.getAvailability();
            String postOffice = selectedSmartPost.getPostOffice();
            String[] openHours = availability.split(",");
            String[] locationInfo = postOffice.split(",");
        /*
            Parsing the information to to a wanted form later to be shown in
            the SmartPost marker on the map.
            */
        String info = city + " ";
        
        for( int i = 0; i < locationInfo.length; i++) {
            info = info + locationInfo[i] + " ";
        }
        info = info + "Avoinna:";
        
        for(int j = 0; j < openHours.length; j++) {
            info = info + openHours[j] + " ";
        }
        
        String location = "'" + address + ", " + postalCode + " " + city + "'" + ",'" + info + "', 'blue'";
        String command = "document.goToLocation(" + location + ")";
       web.getEngine().executeScript(command);
       addedLocations.add(selectedSmartPost);
        }
        else{
            for(int k = 0;k < addedLocations.size(); k++) {
                /*
                Checks if the selected SmartPost is already drawn on the map.
                With this we can avoid same SmartPost been created multiple times
                */
                
                boolean containsSelected = addedLocations.contains(selectedSmartPost);
                
                if(containsSelected == false ) {
                    String postalCode = selectedSmartPost.getPostalCode();
        String address = selectedSmartPost.getAddress();
        String city = selectedSmartPost.getCity();
        String availability = selectedSmartPost.getAvailability();
        String postOffice = selectedSmartPost.getPostOffice();
        String[] openHours = availability.split(",");
        String[] locationInfo = postOffice.split(",");
        
        String info = city + " ";
        
        for( int i = 0; i < locationInfo.length; i++) {
            info = info + locationInfo[i] + " ";
        }
        info = info + "Avoinna:";
        
        for(int j = 0; j < openHours.length; j++) {
            info = info + openHours[j] + " ";
        }
        
        String location = "'" + address + ", " + postalCode + " " + city + "'" + ",'" + info + "', 'blue'";
        String command = "document.goToLocation(" + location + ")";
       web.getEngine().executeScript(command);
       addedLocations.add(selectedSmartPost);
                }
            }
        }
    }
    

    @FXML
    private void createPackageAction(ActionEvent event) {
        /*
        Fires up the the windpw where user can create new packages. Same time sends
        information to FXMLCreatePackage controller about the available SmartPosts.
        */
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("FXMLCreatePackage.fxml"));
            Parent root = (Parent) loader.load();
            FXMLCreatePackageController controller = loader.<FXMLCreatePackageController>getController();
            controller.updateLocations(addedLocations);
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.show();
            
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void clearMapActiom(ActionEvent event) {
        /*
        Erases all drawn routes from the map.
        */
        web.getEngine().executeScript("document.deletePaths()");
    }

public String getColor() {
    // Returns the chosen color.
    String color = "";
    if(colorCombo.valueProperty().getValue().equals("Punainen") ) {
        color = "red";
    }
    if(colorCombo.valueProperty().getValue().equals("Sininen")) {
        color  = "blue";
    }
    if(colorCombo.valueProperty().getValue().equals("Musta")) {
        color = "black";
    }
    if(colorCombo.valueProperty().getValue().equals("Vihreä")) {
        color = "green";
    }
    return color;
}
    @FXML
    private void sendPackageAction(ActionEvent event) {
        boolean a = packagesCombo.valueProperty().getValue() == null;
        boolean b = colorCombo.valueProperty().getValue() == null;
        /*
        Makes sure that the user has selected package he/she wants to send.
        */
        if(a == false && b == false) {
            /*
            Takes needed parametres and executes the script to draw the route
            between the two SmartPosts.
            */
            Package selectedPackage = packagesCombo.valueProperty().getValue();
            float dLat = selectedPackage.getDepartureLocation().getLatitude();
            float dLng = selectedPackage.getDepartureLocation().getLongitude();
            float aLat = selectedPackage.getArrivalLocation().getLatitude();
            float aLng = selectedPackage.getArrivalLocation().getLongitude();
            
            ArrayList<Float> coordinates = new ArrayList<>();
            coordinates.add(dLat);
            coordinates.add(dLng);
            coordinates.add(aLat);
            coordinates.add(aLng);
            
            String command = "document.createPath(" + coordinates + ",'" + getColor() + "',"
                    + selectedPackage.getPostClass() + ")";
            web.getEngine().executeScript(command);
            /*
            Removes the package from the Storage. Clears the ComboBox containing
            the packages and loads the new updated list of packages to ComboBox.
            */
            Storage storage = null;
            storage = Storage.getInstance();
            storage.removePackageFromStorage(selectedPackage);
            packagesCombo.getItems().clear();
            
            ArrayList<Package> updatedList;
            updatedList = storage.getPackageList();
            for(int i = 0; i < updatedList.size(); i++) {
                packagesCombo.getItems().add(updatedList.get(i));
            }
            
        }
    }

    @FXML
    private void updatePackagesAction(ActionEvent event) {
        /*
        Updates the available packages from Storage to a ComboBox
        */
        Storage storage = null;
        storage = Storage.getInstance();
        ArrayList<Package> storageContent = storage.getPackageList();
        packagesCombo.getItems().clear();
        for(int i = 0; i < storageContent.size(); i++) {
            packagesCombo.getItems().add(storageContent.get(i));
        }
    }

    @FXML
    private void showStatistics(ActionEvent event) {
        /*
        Shows user statistics about packages in the storage and packages that
        have been send during the run of the program.
        */
        Stage stage = new Stage();
        try {
            Parent page = FXMLLoader.load(getClass().getResource("FXMLStatistics.fxml"));
            Scene scene = new Scene(page);
            stage.setScene(scene);
            stage.show();
            
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @FXML
    private void closeAndPrintReceiptAction(ActionEvent event) {
        /*
        Closes the program and displays in window the packages that were left to
        Storage and the packages which were send during the run of the program.
        Also writes the same information to "receipt.txt" file.
        */
        try {
            FileWriter fw = new FileWriter("receipt.txt", true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter pw = new PrintWriter(bw);
            
            Storage storage = null;
            storage = Storage.getInstance();
            
            ArrayList<Package> receiptHistory = storage.getPackageHistory();
            ArrayList<Package> receiptStorage = storage.getPackageList();
            int historySize = receiptHistory.size();
            int storageSize = receiptStorage.size();
            Package pkg;
            SmartPost sp1;
            SmartPost sp2;
            
            pw.println("Lähetettiin yhteensä " + historySize + " pakettia.");
            pw.println("Lähetetyt paketit ja niiden tiedot:");
            
            for(int i = 0; i < historySize; i++) {
                pkg = receiptHistory.get(i);
                sp1 = pkg.getDepartureLocation();
                sp2 = pkg.getArrivalLocation();
                String city1 = sp1.getCity();
                String city2 = sp2.getCity();
                String address1 = sp1.getAddress();
                String address2 = sp2.getAddress();
                float distance = pkg.getDistance();
                String name = pkg.getItem().getName();
                String size = pkg.getItem().getSize();
                String weight = pkg.getItem().getWeight();
                String toughness = pkg.getItem().getToughness();
               int postClass = pkg.getPostClass();
                
                pw.println("Mistä: " + city1 + " " + address1);
                pw.println("Minne: " + city2 + " " + address2);
                pw.println("Etäisyys: " + distance + "km");
                pw.println("Pakettiluokka: " + postClass);
                pw.println("Sisältö: " + name + " (" + toughness + ")");
                pw.println("Koko: " + size);
                pw.println("Painoluokka: " + weight);
                pw.println("__________________________________________________");
            }
            
            pw.println();
            pw.println("Varastoon jäi yhteensä " + storageSize + " pakettia.");
            pw.println("Varastoon jääneet paketit ja niiden tiedot:");
            
            for(int j = 0; j < storageSize; j++) {
                pkg = receiptStorage.get(j);
                sp1 = pkg.getDepartureLocation();
                sp2 = pkg.getArrivalLocation();
                String city1 = sp1.getCity();
                String city2 = sp2.getCity();
                String address1 = sp1.getAddress();
                String address2 = sp2.getAddress();
                float distance = pkg.getDistance();
                String name = pkg.getItem().getName();
                String size = pkg.getItem().getSize();
                String weight = pkg.getItem().getWeight();
                String toughness = pkg.getItem().getToughness();
               int postClass = pkg.getPostClass();
                
                pw.println("Mistä: " + city1 + " " + address1);
                pw.println("Minne: " + city2 + " " + address2);
                pw.println("Etäisyys: " + distance + "km");
                pw.println("Pakettiluokka: " + postClass);
                pw.println("Sisältö: " + name + " (" + toughness + ")");
                pw.println("Koko: " + size);
                pw.println("Painoluokka: " + weight);
                pw.println("__________________________________________________");
            }
            pw.close();
            
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        Stage stage = new Stage();
        try {
            Parent page = FXMLLoader.load(getClass().getResource("FXMLReceipt.fxml"));
            Scene scene = new Scene(page);
            stage.setScene(scene);
            stage.show();
            Stage stage2 = (Stage) closeAndPrintReceiptButton.getScene().getWindow();
        stage2.close();
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
